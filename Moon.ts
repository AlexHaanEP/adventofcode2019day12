export default class Moon {

    //Position of the Moon in 3-dimensions
    private position: number[] = [];

    //Velocity of the Moon in 3-dimensions
    private velocity: number[] = [];

    constructor(xPos: number, yPos: number, zPos: number) {
        this.setXPos(xPos);
        this.setYPos(yPos);
        this.setZPos(zPos);
        this.setXVel(0);
        this.setYVel(0);
        this.setZVel(0);
    };

    public getPosition(): number[] {
        return this.position;
    }

    public setPosition(axis: number, value: number) {
        this.position[axis] = value;
    }

    public getVelocity(): number[] {
        return this.velocity;
    }

    public setVelocity(axis: number, value: number) {
        this.velocity[axis] = value;
    }

    public setXPos(x: number): void {
        this.position[0] = x;
    };

    public setYPos(y: number): void {
        this.position[1] = y;
    };

    public setZPos(z: number): void {
        this.position[2] = z;
    };

    public setXVel(x: number): void {
        this.velocity[0] = x;
    };

    public setYVel(y: number): void {
        this.velocity[1] = y;
    };

    public setZVel(z: number): void {
        this.velocity[2] = z;
    };

    public getXPos(): number {
        return this.position[0];
    };

    public getYPos(): number {
        return this.position[1];
    };

    public getZPos(): number {
        return this.position[2];
    };

    public getXVel(): number {
        return this.velocity[0];
    };

    public getYVel(): number {
        return this.velocity[1];
    };

    public getZVel(): number {
        return this.velocity[2];
    };

}