import Moon from './Moon';

const steps: number = 1000;
let totalEnergy: number = 0;
let moons: Moon[] = [];
let stdin = process.openStdin();

stdin.addListener("data", (d) => {
    let input: string = d.toString().replace(/[<\s]/g, "");
    input = input.toString().replace(/[>]/g, ',');
    input = input.toString().replace(/[xyz=]/g, '');
    let coordinates: string[] = input.toString().split(',');
    let tmp: number[] = [];
    for (let i: number = 0; i <= coordinates.length - 1; i++) {
        tmp.push(+coordinates[i]);
        if (tmp.length % 3 === 0) {
            moons.push(new Moon(tmp[0] as number, tmp[1] as number, tmp[2] as number));
            tmp = [];
        }
    }
    main();
});

function main(): void {
    let i: number = 1;
    while (i <= steps) {
        applyGravity();
        applyVelocity();
        i++;
    }
    calculateTotalEnergy();
}

function applyGravity():void {
    for ( let i: number = 0; i <= moons.length - 2; i++) {
        let currentMoon: Moon = moons[i];
        for (let j: number = i+1; j <= moons.length - 1; j++) {
            let pairMoon: Moon = moons[j];
            if (i !== j) {
                comparePosition(currentMoon, pairMoon);
            }
        }
    }
}

function comparePosition(currentMoon: Moon, pairMoon: Moon): void {
    for (let axis: number = 0; axis < 3; axis++) {
        if (currentMoon.getPosition()[axis] > pairMoon.getPosition()[axis]) {
            currentMoon.setVelocity(axis, currentMoon.getVelocity()[axis] - 1);
            pairMoon.setVelocity(axis, pairMoon.getVelocity()[axis] + 1);
        } else if (currentMoon.getPosition()[axis] < pairMoon.getPosition()[axis]) {
            currentMoon.setVelocity(axis, currentMoon.getVelocity()[axis] + 1);
            pairMoon.setVelocity(axis, pairMoon.getVelocity()[axis] - 1);
        }
    }
}

function applyVelocity():void {
    for (let i in moons) {
        let currentMoon: Moon = moons[i];
        for (let axis: number = 0; axis < 3; axis++) {
            currentMoon.setPosition(axis, currentMoon.getPosition()[axis]
                + currentMoon.getVelocity()[axis]);
        }
    }
}

function calculateTotalEnergy(): void {
    for (let moon of moons) {
        let potentialEnergy: number = 0;
        let kineticEnergy: number = 0;
        for (let axis: number = 0; axis < 3; axis++) {
            potentialEnergy += Math.abs(moon.getPosition()[axis]);
            kineticEnergy += Math.abs(moon.getVelocity()[axis]);
        }
        totalEnergy += potentialEnergy * kineticEnergy;
    }
    console.log(totalEnergy);
}
